## GitLab Pages Example - How To

This project's Pages are built using the following `.gitlab-ci.yml` file.

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

